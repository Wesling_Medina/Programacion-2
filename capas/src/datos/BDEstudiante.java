/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author Wesling Medina
 */
public class BDEstudiante {

    public void escribir(String datos) {
        try {
            File archivo = new File("estudiantes.txt");
            try (FileWriter archi = new FileWriter(archivo, true)) {
                archi.write(datos);
                archi.close();
            }
        } catch (IOException e) {

        }
    }

    public String[] leer() {
        String[] marcas = new String[10];
        try {
            FileReader read = new FileReader("Marcas.txt");
            BufferedReader conten = new BufferedReader(read);
            String texto = "";
            int cont = 0;
            while (conten.ready()) {
                texto = conten.readLine();
                marcas[cont] = texto;
                cont++;
            }
            read.close();
            conten.close();
        } catch (IOException e) {
            System.out.println("Error al escribir en el archivo y LEERLO" + e);
        }
        return marcas;
    }
}
