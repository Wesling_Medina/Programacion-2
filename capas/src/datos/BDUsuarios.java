/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package datos;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/**
 *
 * @author Wesling Medina
 */
public class BDUsuarios {
 
    public String leerArchivo(){
        String userData = "";
        try {
            FileReader read = new FileReader("usuarios.txt");
            BufferedReader conten = new BufferedReader(read);
            String texto = "";
            int cont = 0;
            while (conten.ready()) {
                texto = conten.readLine();
                userData += texto;
                cont++;
            }
            read.close();
            conten.close();
        } catch (IOException e) {
            System.out.println("Error al escribir en el archivo y LEERLO" + e);
        }
        return userData;
    }
}
