/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;

import datos.*;

/**
 *
 * @author Wesling Medina
 */
public class Estudiante {

    String primera;
    String segunda;
    String tercera;
    String nombre;
    String apellido;
    BDEstudiante bDEstudiante = new BDEstudiante();

    public Estudiante() {

    }

    public Estudiante(String primera, String segunda, String tercera, String nombre, String apellido) {
        this.primera = primera;
        this.segunda = segunda;
        this.tercera = tercera;
        this.nombre = nombre;
        this.apellido = apellido;
    }

    public void guardar() {
        bDEstudiante.escribir(nombre
                + "," + apellido
                + "," + primera
                + "," + segunda
                + "," + tercera
                + "\n");
    }
    public String getPrimera() {
        return primera;
    }

    public void setPrimera(String primera) {
        this.primera = primera;
    }

    public String getSegunda() {
        return segunda;
    }

    public void setSegunda(String segunda) {
        this.segunda = segunda;
    }

    public String getTercera() {
        return tercera;
    }

    public void setTercera(String tercera) {
        this.tercera = tercera;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellido() {
        return apellido;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

}
