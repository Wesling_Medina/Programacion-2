/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package logica;
import datos.*;
import javax.swing.JOptionPane;
/**
 *
 * @author Wesling Medina
 */
public class Usuarios {
    char[] contra;
    String user;
    BDUsuarios datos;

    public Usuarios() {
        datos = new BDUsuarios();
    }

    public Usuarios(char [] contra, String user) {
        this.contra = contra;
        this.user = user;
    }
    
    public boolean coincide(){
        String[] datosStrings = datos.leerArchivo().split(",");
        String userString = datosStrings[0];
        String pass = datosStrings[1];
        String contraString = "";
        for(int i = 0; i < contra.length; i++){
            contraString += contra[i];
        }
        if(userString.equals(user) && contraString.equals(pass)){
            return true;
        }
        return false;
    }
    public char[] getContra() {
        return contra;
    }

    public void setContra(char [] contra) {
        this.contra = contra;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }   
    
}
