/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package productos;

/**
 *
 * @author Wesling Medina
 */
public class Productos {

    public String fCaducidad;
    public int lote;
    public String pOrigen;
    public String fEnvasado;

    public Productos() {
    }

    public Productos(String fCaducidad, int lote, String pOrigen, String fEnvasado) {
        this.fCaducidad = fCaducidad;
        this.lote = lote;
        this.pOrigen = pOrigen;
        this.fEnvasado = fEnvasado;
    }
    
    public String getDatos(){
        return "Fecha caducidad: " + fCaducidad
                + "\nNúmero lote: " + lote
                + "\nPaís origen: " + pOrigen
                +"\nFecha envasado: " + fEnvasado;
    }
    public String getpOrigen() {
        return pOrigen;
    }

    public void setpOrigen(String pOrigen) {
        this.pOrigen = pOrigen;
    }

    public String getfCaducidad() {
        return fCaducidad;
    }

    public void setfCaducidad(String fCaducidad) {
        this.fCaducidad = fCaducidad;
    }

    public int getLote() {
        return lote;
    }

    public void setLote(int lote) {
        this.lote = lote;
    }

    public String getfEnvasado() {
        return fEnvasado;
    }

    public void setfEnvasado(String fEnvasado) {
        this.fEnvasado = fEnvasado;
    }

}
