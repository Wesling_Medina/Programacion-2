/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package productos;

/**
 *
 * @author Wesling Medina
 */
public class Frescos extends Productos {

    public Frescos() {
    }

    public Frescos(String fCaducidad, int lote, String pOrigen, String fEnvasado) {
        super(fCaducidad, lote, pOrigen, fEnvasado);
    }

    public String getDatos() {
        return "Fecha caducidad: " + fCaducidad
                + "\nNúmero lote: " + lote
                + "\nPaís origen: " + pOrigen
                + "\nFecha envasado: " + fEnvasado;
    }
}
