/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package productos;

/**
 *
 * @author Wesling Medina
 */
public class Main {

    public static void main(String[] args) {
        Productos p = new Productos("30/10/2018", 12, "Chile", "28/11/2017");
        Frescos frescos = new Frescos("30/10/2020", 11, "Argentina", "24/1/2010");
        Refrigerados refri = new Refrigerados("1/1/2030", 10, "Alaska", "2/2/2010", 1234, 32);
        CAire aire = new CAire(30, 23, 11, 8, "30/1/2019", 1, "Alemania", "1/10/2018", 12);
        CAgua agua = new CAgua(12, "30/1/2020", 2, "Austria", "2/2/2010", 29);
        CNitrogeno nitro = new CNitrogeno("Nitrogeno", 5, "30/11/2018", 2, "Italia", "2/2/2008", 32);
        datos(p);
        System.out.println("");
        System.out.println("Producto fresco");
        datos(frescos);
        System.out.println("");
        System.out.println("Producto refrigerados");
        datos(refri);
        System.out.println("");
        datos(aire);
        System.out.println("");
        datos(agua);
        System.out.println("");
        datos(nitro);

    }

    public static void datos(Productos p) {
        System.out.println(p.getDatos());
    }
}
