/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package productos;

/**
 *
 * @author Wesling Medina
 */
public class Refrigerados extends Productos {

    private int cOSA;
    private int tRecomendada;

    public Refrigerados() {
    }

    public Refrigerados(String fCaducidad, int lote, String pOrigen, String fEnvasado, int cOSA, int tRecomendada) {
        super(fCaducidad, lote, pOrigen, fEnvasado);
        this.cOSA = cOSA;
        this.tRecomendada = tRecomendada;
    }

    public String getDatos() {
        return "Fecha caducidad: " + fCaducidad
                + "\nNúmero lote: " + lote
                + "\nPaís origen: " + pOrigen
                + "\nFecha envasado: " + fEnvasado
                + "\nCódigo de supervisión alimentaria: " + cOSA
                + "\nTemperatura recomendada: " + tRecomendada;
    }

    public int getcOSA() {
        return cOSA;
    }

    public void setcOSA(int cOSA) {
        this.cOSA = cOSA;
    }

    public int gettRecomendada() {
        return tRecomendada;
    }

    public void settRecomendada(int tRecomendada) {
        this.tRecomendada = tRecomendada;
    }
}
