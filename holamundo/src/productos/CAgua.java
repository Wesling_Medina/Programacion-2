/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package productos;

/**
 *
 * @author Wesling Medina
 */
public class CAgua extends Congelados {

    private float salinidad;

    public CAgua(float salinidad, String fCaducidad, int lote, String pOrigen, String fEnvasado, int tRecomendada) {
        super(fCaducidad, lote, pOrigen, fEnvasado, tRecomendada);
        this.salinidad = salinidad;
    }

    public String getDatos() {
        return "CONGELADOS CON AGUA."
                + "\nFecha caducidad: " + fCaducidad
                + "\nNúmero lote: " + lote
                + "\nPaís origen: " + pOrigen
                + "\nFecha envasado: " + fEnvasado
                + "\nSalinidad: " + salinidad;
    }

    public float getSalinidad() {
        return salinidad;
    }

    public void setSalinidad(float salinidad) {
        this.salinidad = salinidad;
    }

}
