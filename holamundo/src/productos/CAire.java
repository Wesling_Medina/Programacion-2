/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package productos;

/**
 *
 * @author Wesling Medina
 */
public class CAire extends Congelados {

    //(% de nitrógeno, % de oxígeno, % de dióxido de carbono y % de
    //vapor de agua).
    private int pNitro;
    private int pOxigeno;
    private int pDioCar;
    private int pVA;

    public CAire(int pNitro, int pOxigeno, int pDioCar, int pVA, String fCaducidad, int lote, String pOrigen, String fEnvasado, int tRecomendada) {
        super(fCaducidad, lote, pOrigen, fEnvasado, tRecomendada);
        this.pNitro = pNitro;
        this.pOxigeno = pOxigeno;
        this.pDioCar = pDioCar;
        this.pVA = pVA;
    }

    public String getDatos() {
        return "CONGELADOS CON AIRE."
                + "\nFecha caducidad: " + fCaducidad
                + "\nNúmero lote: " + lote
                + "\nPaís origen: " + pOrigen
                + "\nFecha envasado: " + fEnvasado
                + "\nPorcentaje Nitrogeno: " + pNitro
                + "\nPorcentaje Oxigeno: " + pOxigeno
                +"\nPorcentaje de dioxido de carbono: " + pDioCar
                + "\nPorcentaje de vapor de agua: " + pVA;
    }

    public int getpNitro() {
        return pNitro;
    }

    public void setpNitro(int pNitro) {
        this.pNitro = pNitro;
    }

    public int getpOxigeno() {
        return pOxigeno;
    }

    public void setpOxigeno(int pOxigeno) {
        this.pOxigeno = pOxigeno;
    }

    public int getpDioCar() {
        return pDioCar;
    }

    public void setpDioCar(int pDioCar) {
        this.pDioCar = pDioCar;
    }

    public int getpVA() {
        return pVA;
    }

    public void setpVA(int pVA) {
        this.pVA = pVA;
    }

}
