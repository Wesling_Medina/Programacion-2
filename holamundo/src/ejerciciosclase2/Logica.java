/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejerciciosclase2;

/**
 *
 * @author Wesling Medina
 */
public class Logica {
    private int num;
    private int random;
    private int tries;
    public Logica() {
        num = 0;
        random = 0;
        tries = 5;
    }
    public int random(){
        random = (int)(Math.random()*20)+1;
        return random;
    }
    public int intentos(){
        tries-=1;
        return tries;
    }
    public int adivina(){
        if(num == random){
            return 1;
        }else if(num < random){
            return 2;
        }
        return 3;
    }
    
    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }
}
