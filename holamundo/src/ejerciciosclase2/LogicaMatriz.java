/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejerciciosclase2;

/**
 *
 * @author Wesling Medina
 */
public class LogicaMatriz {

    private Persona[][] persona;

    public LogicaMatriz() {
        persona = new Persona[2][2];
    }

    public LogicaMatriz(Persona[][] persona) {
        this.persona = persona;
    }

    public Persona[][] getPersona() {
        return persona;
    }

    public void capturarDatos(Persona persona) {
        APP:
        for (int i = 0; i < this.persona.length; i++) {
            for (int j = 0; j < this.persona[i].length; j++) {
                if (this.persona[i][j] == null) {
                    this.persona[i][j] = persona;
                    break APP;
                }
            }
        }
    }
    
    public Persona[][] imprimir(){
        return persona;
    }
    public void setPersona(Persona[][] persona) {
        this.persona = persona;
    }

}
