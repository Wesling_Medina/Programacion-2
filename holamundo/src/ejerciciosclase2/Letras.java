/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejerciciosclase2;

import javax.swing.JOptionPane;

/**
 *
 * @author Wesling Medina
 */
public class Letras {

    public static void main(String[] args) {
        LetrasLogica lLogica = new LetrasLogica();
        while (true) {
            String primera = JOptionPane.showInputDialog("Digite la primera palabra: ");
            String segunda = JOptionPane.showInputDialog("Digite la segunda palabra: ");
            System.out.println(lLogica.rimas(primera, segunda));
            String preg = JOptionPane.showInputDialog("1) Probar otras palabras.\n"
                    + "2) Salir.");
            if(preg.equalsIgnoreCase("2")){
                break;
            }
        }
    }
}
