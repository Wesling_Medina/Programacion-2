/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejerciciosclase2;

import java.util.HashSet;
import javax.swing.JOptionPane;

/**
 *
 * @author Wesling Medina
 */
public class MainMatriz {

    public static void main(String[] args) {
        LogicaMatriz log = new LogicaMatriz();
        while (true) {
            boolean isExiste = false;
            SALIR:
            while (true) {
                String nombre = JOptionPane.showInputDialog("Digite el nombre: ");
                for (int i = 0; i < log.imprimir().length; i++) {
                    for (int j = 0; j < log.imprimir()[i].length; j++) {
                        if (log.imprimir()[i][j] != null) {
                            if (log.imprimir()[i][j].getNombre().equalsIgnoreCase(nombre)) {
                                isExiste = true;
                            }
                        }
                    }
                }
                if (!isExiste) {
                    int edad = Integer.parseInt(JOptionPane.showInputDialog("Digite la edad: "));
                    log.capturarDatos(new Persona(nombre, edad));
                    String preg = JOptionPane.showInputDialog("1) Agregar más personas.\n"
                            + "2) No agregar más personas.");
                    if (preg.equalsIgnoreCase("2")) {
                        break;
                    }
                }else{
                    isExiste = false;
                }
            }
            for (int i = 0; i < log.imprimir().length; i++) {
                for (int j = 0; j < log.imprimir()[i].length; j++) {
                    if (log.imprimir()[i][j] != null) {
                        if (log.imprimir()[i][j].getEdad() % 2 != 0) {
                            System.out.print(log.imprimir()[i][j].getNombre() + "  ");
                            System.out.println("\033[31m" + log.imprimir()[i][j].getEdad());
                            System.out.println("\033[37m");
                        } else {
                            System.out.print(log.imprimir()[i][j].getNombre() + "  ");
                            System.out.println("\033[34m" + log.imprimir()[i][j].getEdad());
                            System.out.println("\033[37m");
                        }
                    }
                }
            }
            String preg = JOptionPane.showInputDialog("1) Cambiar edad.\n"
                    + "2) No cambiar edad.");
            if (preg.equalsIgnoreCase("1")) {
                String nombre = JOptionPane.showInputDialog("Digite el nombre: ");
                APPLICATION:
                for (int i = 0; i < log.imprimir().length; i++) {
                    for (int j = 0; j < log.imprimir()[i].length; j++) {
                        if (log.imprimir()[i][j].getNombre().equalsIgnoreCase(nombre)) {
                            int edad = Integer.parseInt(JOptionPane.showInputDialog("Digite la edad: "));
                            log.imprimir()[i][j].setEdad(edad);
                            break APPLICATION;
                        }
                    }
                }
                System.out.println("Nuevos datos.");
                for (int i = 0; i < log.imprimir().length; i++) {
                    for (int j = 0; j < log.imprimir()[i].length; j++) {
                        if (log.imprimir()[i][j] != null) {
                            if (log.imprimir()[i][j].getEdad() % 2 != 0) {
                                System.out.print(log.imprimir()[i][j].getNombre() + "  ");
                                System.out.println("\033[31m" + log.imprimir()[i][j].getEdad());
                                System.out.println("\033[37m");
                            } else {
                                System.out.print(log.imprimir()[i][j].getNombre() + "  ");
                                System.out.println("\033[34m" + log.imprimir()[i][j].getEdad());
                                System.out.println("\033[37m");
                            }
                        }
                    }
                }
            }
            String opcion = JOptionPane.showInputDialog("1) Salir.\n"
                    + "2) Agregar más persona.");
            if (opcion.equalsIgnoreCase("1")) {
                break;
            }
        }
    }
}
