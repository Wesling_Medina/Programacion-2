/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejerciciosclase2;

import javax.swing.JOptionPane;

/**
 *
 * @author Wesling Medina
 */
public class Main {

    public static void main(String[] args) {
        System.out.println("Adivine un numero entre 1 y 20.");
        Logica log = new Logica();
        log.random();
        APP:
        while (true) {
            int num = Integer.parseInt(JOptionPane.showInputDialog("Digite un número entre 1 y 20: "));
            log.setNum(num);
            switch (log.adivina()) {
                case 1:
                    System.out.println("Ganó");
                    break APP;
                case 2:
                    System.out.println("Más.");
                    break;
                default:
                    System.out.println("Menos.");
                    break;
            }
            int tries = log.intentos();
            if(tries == 0){
                System.out.println("Perdió");
                break;
            }else{
                System.out.println("Le quedan " + tries + " intentos.");
            }
        }
    }
}
