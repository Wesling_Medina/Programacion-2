/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejerciciosclase2;

/**
 *
 * @author Wesling Medina
 */
public class LetrasLogica {

    public LetrasLogica() {

    }

    public String rimas(String primera, String segunda) {
        String first = primera.substring((primera.length()) - 3, primera.length());
        String second = segunda.substring((segunda.length()) - 3, segunda.length());
        String first2 = primera.substring((primera.length()) - 2, primera.length());
        String second2 = segunda.substring((segunda.length()) - 2, segunda.length());
        if (first.equalsIgnoreCase(second)) {
            return "Riman";
        } else if (first2.equalsIgnoreCase(second2)) {
            return "Riman un poco";
        } else {
            return "No riman";
        }
    }
}
