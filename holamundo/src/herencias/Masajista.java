/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package herencias;

/**
 *
 * @author Wesling Medina
 */
public class Masajista extends SeleccionFutbol {

    private String titulacion;
    private int anosExperiencia;

    public Masajista() {

    }

    public Masajista(String titulacion, int anosExperiencia, int id, String nombre, String apellidos, int edad) {
        super(id, nombre, apellidos, edad);
        this.titulacion = titulacion;
        this.anosExperiencia = anosExperiencia;
    }

    public boolean darMasaje() {
        if (anosExperiencia < 50 && titulacion.equalsIgnoreCase("Si")) {
            return true;
        }
        return false;
    }

    public String getDatos() {
        return "Titulacion: " + titulacion
                + "\nAños de experiencia: " + anosExperiencia
                + "\nId: " + id
                + "\nNombre: " + nombre
                + "\nApellido: " + apellidos
                + "\nEdad: " + edad;
    }

    public String concentrarse() {
        return "Sin concentración";
    }

    public String viajar() {
        return "No puede viajar con el equipo.";
    }

    public String getTitulacion() {
        return titulacion;
    }

    public void setTitulacion(String titulacion) {
        this.titulacion = titulacion;
    }

    public int getAnosExperiencia() {
        return anosExperiencia;
    }

    public void setAnosExperiencia(int anosExperiencia) {
        this.anosExperiencia = anosExperiencia;
    }

}
