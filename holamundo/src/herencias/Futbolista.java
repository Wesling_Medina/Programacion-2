/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package herencias;

/**
 *
 * @author Wesling Medina
 */
public class Futbolista extends SeleccionFutbol {

    private int dorsal;
    private String demarcacion;//Posición donde juega

    public Futbolista() {
    }

    public Futbolista(int id, String nombre, String apellidos, int edad, int dorsal, String demarcacion) {
        super(id, nombre, apellidos, edad);
        this.dorsal = dorsal;
        this.demarcacion = demarcacion;
    }

    public boolean jugarPartido() {
        if (dorsal > 15) {
            return false;
        }
        return true;
    }

    public boolean entrenar() {
        if (demarcacion.equalsIgnoreCase("portero")) {
            return false;
        }
        return true;
    }

    public String concentrarse() {
        return "Concentracion dos horas en el campo de juego.";
    }

    public String viajar() {
        if (dorsal == 7 || dorsal == 10 || dorsal == 11 || dorsal == 9 && edad >= 26) {
            return "Puede viajar.";
        }
        return "No puede viajar.";
    }

    public String getDatos() {
        return "Dorsal: " + dorsal
                + "\nPosición: " + demarcacion
                + "\nId: " + id
                + "\nNombre: " + nombre
                + "\nApellido: " + apellidos
                + "\nEdad: " + edad;
    }

    public int getDorsal() {
        return dorsal;
    }

    public void setDorsal(int dorsal) {
        this.dorsal = dorsal;
    }

    public String getDemarcacion() {
        return demarcacion;
    }

    public void setDemarcacion(String demarcacion) {
        this.demarcacion = demarcacion;
    }

}
