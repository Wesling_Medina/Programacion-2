/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package herencias;

/**
 *
 * @author Wesling Medina
 */
public class MainFutbol {

    public static void main(String[] args) {
        SeleccionFutbol sF = new SeleccionFutbol(1, "Chepe", "Khalifa", 20);
        Futbolista futbolista = new Futbolista(2, "Cristiano", "Ronaldo", 32, 7, "delantero");
        Futbolista futbolista2 = new Futbolista(3, "Karim", "Benzema", 32, 10, "delantero");
        Futbolista futbolista3 = new Futbolista(4, "Sergio", "Ramos", 33, 4, "defensa");
        Futbolista futbolista4 = new Futbolista(5, "Vinicius", "Jr", 19, 28, "delantero");
        Entrenador entrandor = new Entrenador(6, "Zinedine", "Zidane", 45, "12345");
        Masajista masajista = new Masajista("Si", 6, 7, "Mario", "Kart", 29);
        datos(sF);
        System.out.println(sF.concentrarse());
        System.out.println(sF.viajar());
        System.out.println("");

        datos(futbolista);
        concentrarse(futbolista);
        viajar(futbolista);
        System.out.println("");

        datos(futbolista4);
        concentrarse(futbolista4);
        viajar(futbolista4);
        System.out.println("");

        datos(entrandor);
        concentrarse(entrandor);
        viajar(entrandor);
        System.out.println("");

        datos(masajista);
        concentrarse(masajista);
        viajar(masajista);
        System.out.println("");

    }

    public static void concentrarse(SeleccionFutbol sL) {
        System.out.println(sL.concentrarse());
    }

    public static void viajar(SeleccionFutbol sL) {
        System.out.println(sL.viajar());
    }

    public static void datos(SeleccionFutbol sL) {
        System.out.println(sL.getDatos());
    }

}
