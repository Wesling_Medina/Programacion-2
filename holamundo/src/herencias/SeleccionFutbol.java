/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package herencias;

/**
 *
 * @author Wesling Medina
 */
public class SeleccionFutbol {

    int id;
    String nombre;
    String apellidos;
    int edad;

    public SeleccionFutbol() {

    }

    public SeleccionFutbol(int id, String nombre, String apellidos, int edad) {
        this.id = id;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.edad = edad;
    }

    public String concentrarse() {
        return "Sin concentración";
    }

    public String viajar() {
        return "Viaja con el equipo.";
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDatos() {
        return "Puesto: Agüatero."
                + "\nId: " + id
                + "\nNombre: " + nombre
                + "\nApellido: " + apellidos
                + "\nEdad: " + edad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }
}
