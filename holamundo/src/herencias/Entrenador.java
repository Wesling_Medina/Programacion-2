/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package herencias;

/**
 *
 * @author Wesling Medina
 */
public class Entrenador extends SeleccionFutbol {

    private String idFederacion;

    public Entrenador() {

    }

    public Entrenador(int id, String nombre, String apellido, int edad, String idFederacion) {
        super(id, nombre, apellido, edad);
        this.idFederacion = idFederacion;
    }

    public boolean dirigirPartido() {
        if (idFederacion.equalsIgnoreCase("")) {
            return false;
        }
        return true;
    }

    public boolean dirigirEntrenamiento() {
        if (edad > 65) {
            return false;
        }
        return true;
    }

    public String getDatos() {
        return "Federación: " + idFederacion
                + "\nId: " + id
                + "\nNombre: " + nombre
                + "\nApellido: " + apellidos
                + "\nEdad: " + edad;
    }

    public String concentrarse() {
        return "Relajación en la piscina.";
    }

    public String viajar() {
        return "Viaja con el equipo.";
    }

    public String getIdFederacion() {
        return idFederacion;
    }

    public void setIdFederacion(String idFederacion) {
        this.idFederacion = idFederacion;
    }

}
