/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicios;

/**
 *
 * @author Wesling Medina
 */
public class Logica {

    int dia;
    int mes;
    int year;
    String nacimiento;

    public String getNacimiento() {
        return nacimiento;
    }

    public void setNacimiento(String nacimiento) {
        this.nacimiento = nacimiento;
    }

    public Logica() {
        dia = 0;
        mes = 0;
        year = 0;
        nacimiento = "";
    }

    public Logica(int dia, int mes, int year, String nacimiento) {
        this.dia = dia;
        this.mes = mes;
        this.year = year;
        this.nacimiento = nacimiento;
    }

    public void fecha(){
        String[] fechas = nacimiento.split("/");
        dia = Integer.parseInt(fechas[0]);
        mes = Integer.parseInt(fechas[1]);
        year = Integer.parseInt(fechas[2]);
    }
    
    public String suerte(){
        int primera = dia +mes + year;
        int pN = primera % 10;
        primera = primera / 10;
        int sN = primera % 10;
        primera = primera /10;
        int tN = primera % 10;
        primera = primera / 10;
        int cN = primera %10;
        return "La suma es: " + (pN + sN + tN + cN);
    }
    public int getDia() {
        return dia;
    }

    public void setDia(int dia) {
        this.dia = dia;
    }

    public int getMes() {
        return mes;
    }

    public void setMes(int mes) {
        this.mes = mes;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

}
