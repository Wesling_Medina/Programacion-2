/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicios;

/**
 *
 * @author Wesling Medina
 */
public class Perfecto {
    int perfecto;

    public Perfecto() {
    }
    
    
    public String numero(){
        boolean isPerfect = false;
        int suma = 0;
        for(int i = 1; i < perfecto; i++){
            if(perfecto%i == 0){
                suma+=i;
            }
        }
        if(suma == perfecto){
            isPerfect = true;
        }
        if(isPerfect){
            return "El número es perfecto";
        }
        return "No es un número perfecto.";
    }
    public int getPerfecto() {
        return perfecto;
    }

    public void setPerfecto(int perfecto) {
        this.perfecto = perfecto;
    }
}
