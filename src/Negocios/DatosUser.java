/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocios;

import Datos.*;
import java.util.LinkedList;

/**
 *
 * @author Wesling Medina
 */
public class DatosUser {

    private String nombre;
    private String sexo;
    private String cedula;
    private int edad;
    BaseDeDatos bD = new BaseDeDatos();
    LinkedList<String> cedulas = new LinkedList<>();

    public DatosUser() {

    }

    public String dataUsuario() {
        String tempString = "";
        LinkedList<String> temp = new LinkedList<>();
        bD.Conexion();
        bD.traerEditar();
        temp = bD.data();
        for (String datos : temp) {
            String[] tempSplit = datos.split(" ");
            if(tempSplit[0].equalsIgnoreCase(cedula)){
                tempString = datos;
                break;
            }
        }
        return tempString;
    }
    public void update(){
        bD.actualizar(cedula, nombre, edad, sexo);
    }
    public boolean imprimir() {
        boolean isExiste = true;
        bD.Conexion();
        bD.traer();
        cedulas = bD.cedLista();
        for (String ceds : cedulas) {
            System.out.println(ceds);
            if (ceds.equalsIgnoreCase(cedula)) {
                isExiste = false;
                break;
            }
        }
        if (isExiste) {
            bD.insertar(cedula, nombre, edad, sexo);
            return true;
        }
        return false;
    }

    public void editar() {

    }

    public DatosUser(String nombre, String sexo, String cedula, int edad) {
        this.nombre = nombre;
        this.sexo = sexo;
        this.cedula = cedula;
        this.edad = edad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

}
