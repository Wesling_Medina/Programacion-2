/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Negocios;

import java.util.LinkedList;
import Datos.*;

/**
 *
 * @author Wesling Medina
 */
public class consultaUser {

    private String nombre;
    private String sexo;
    private String cedula;
    private int edad;
    private consultaDatos cU = new consultaDatos();
    boolean isTrue = false;
    String tempString = "";

    public consultaUser() {
    }

    public consultaUser(String nombre, String sexo, String cedula, int edad) {
        this.nombre = nombre;
        this.sexo = sexo;
        this.cedula = cedula;
        this.edad = edad;
    }

    public String isExiste() {
        return tempString;
    }

    public boolean dataUsuario() {

        LinkedList<String> temp = new LinkedList<>();
        cU.traerEditar();
        temp = cU.data();
        for (String datos : temp) {
            String[] tempSplit = datos.split(" ");
            if (tempSplit[0].equalsIgnoreCase(cedula)) {
                tempString = datos;
                return true;
            }
        }
        return false;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {

        this.cedula = cedula;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

}
