/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.LinkedList;

/**
 *
 * @author Wesling Medina
 */
public class BaseDeDatos {

    private Connection connection = null;
    private ResultSet rs = null;
    private Statement s = null;
    private LinkedList<String> cedulas = new LinkedList<>();
    private LinkedList<String> datos = new LinkedList<>();

    public void Conexion() {
        if (connection != null) {
            return;
        }

        String url = "jdbc:postgresql://localhost:5432/postgres";
        String password = "12345";
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(url, "postgres", password);
            if (connection != null) {
                System.out.println("Connecting to database...");
            }
        } catch (Exception e) {
            System.out.println("Problem when connecting to the database");
        }
    }

    public void insertar(String cedula, String nombre, int edad, String sexo) {//GEN-FIRST:event_btnInsertarActionPerformed
        Conexion();
        try {
            s = connection.createStatement();
            int z = s.executeUpdate("INSERT INTO usuarios (cedula, nombre, edad, sexo) VALUES ('" + cedula + "', '" + nombre + "', '" + edad + "', '" + sexo + "')");
            if (z == 1) {
                System.out.println("Se agregó el registro de manera exitosa");
            } else {
                System.out.println("Error al insertar el registro");
            }
        } catch (Exception e) {
            System.out.println("Error de conexión");
        }
    }

    public void actualizar(String cedula, String nombre, int edad, String sexo) {
        Conexion();
        try {
            s = connection.createStatement();
            int z = s.executeUpdate("UPDATE usuarios SET nombre = '" + nombre + "', edad = '" + edad + "', sexo = '" + sexo + "' WHERE cedula = '" + cedula + "'");
            if (z == 1) {
                System.out.println("Se agregó el registro de manera exitosa");
            } else {
                System.out.println("Error al insertar el registro");
            }
        } catch (Exception e) {
            System.out.println("Error de conexión");
        }
    }

    public void traer() {
        Conexion();
        try {
            s = connection.createStatement();
            rs = s.executeQuery("SELECT cedula FROM usuarios");

            while (rs.next()) {
                cedulas.add(rs.getString("cedula"));
            }
        } catch (Exception e) {
            System.out.println("Error de conexión");
        }
    }

    public void traerEditar() {
        Conexion();
        try {
            s = connection.createStatement();
            rs = s.executeQuery("SELECT cedula, nombre, edad, sexo FROM usuarios");
            while (rs.next()) {
                datos.add(rs.getString("cedula")
                        + " " + rs.getString("nombre")
                        + " " + rs.getString("edad")
                        + " " + rs.getString("sexo")
                );
            }
        } catch (Exception e) {
            System.out.println("Error de conexión");
        }
    }

    public LinkedList<String> cedLista() {
        return cedulas;
    }

    public LinkedList<String> data() {
        return datos;
    }
}
