/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

/**
 *
 * @author Wesling Medina
 */
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.LinkedList;

/**
 *
 * @author Wesling Medina
 */
public class consultaDatos {

    private Connection connection = null;
    private ResultSet rs = null;
    private Statement s = null;
    private LinkedList<String> datos = new LinkedList<>();

    public void Conexion() {
        if (connection != null) {
            return;
        }

        String url = "jdbc:postgresql://localhost:5432/postgres";
        String password = "12345";
        try {
            Class.forName("org.postgresql.Driver");
            connection = DriverManager.getConnection(url, "postgres", password);
            if (connection != null) {
                System.out.println("Connecting to database...");
            }
        } catch (Exception e) {
            System.out.println("Problem when connecting to the database");
        }
    }

    public void traerEditar() {
        Conexion();
        try {
            s = connection.createStatement();
            rs = s.executeQuery("SELECT cedula, nombre, edad, sexo FROM usuarios");
            while (rs.next()) {
                datos.add(rs.getString("cedula")
                        + " " + rs.getString("nombre")
                        + " " + rs.getString("edad")
                        + " " + rs.getString("sexo")
                );
            }
        } catch (Exception e) {
            System.out.println("Error de conexión");
        }
    }

    public LinkedList<String> data() {
        return datos;
    }
}
