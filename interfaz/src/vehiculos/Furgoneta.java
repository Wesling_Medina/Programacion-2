/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vehiculos;

/**
 *
 * @author Wesling Medina
 */
public class Furgoneta extends Vehiculo{
    int capacidad;
    boolean isDisponible;

    public Furgoneta() {
    }

    public Furgoneta(int capacidad, boolean isDisponible) {
        this.capacidad = capacidad;
        this.isDisponible = isDisponible;
    }

    public Furgoneta(int capacidad, boolean isDisponible, String marca, String color, int precio, String placa) {
        super(marca, color, precio, placa);
        this.capacidad = capacidad;
        this.isDisponible = isDisponible;
    }

    
    
    public int getCapacidad() {
        return capacidad;
    }

    public void setCapacidad(int capacidad) {
        this.capacidad = capacidad;
    }

    public boolean isIsDisponible() {
        return isDisponible;
    }

    public void setIsDisponible(boolean isDisponible) {
        this.isDisponible = isDisponible;
    }
    
    
}
