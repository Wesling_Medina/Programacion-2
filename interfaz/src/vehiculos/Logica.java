/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vehiculos;

import java.io.*;
import java.util.LinkedList;

/**
 *
 * @author Wesling Medina
 */
public class Logica {

    private Furgoneta[] furgonetas;
    private Deportivo[] deportivos;

    public Logica() {
        furgonetas = new Furgoneta[20];
        deportivos = new Deportivo[20];
    }

    public void creaMarcas() {
        try {
            File archivo = new File("Marcas.txt");
            try (FileWriter archi = new FileWriter(archivo)) {
                archi.write("Volswagen\n"
                        + "Toyota\n"
                        + "Lam\n"
                        + "Mustang\n"
                        + "Ferrari\n"
                        + "Bentley");
                archi.close();
            }
        } catch (IOException e) {

        }
    }

    public void creaColores() {
        try {
            File archivo = new File("Colores.txt");
            try (FileWriter archi = new FileWriter(archivo)) {
                archi.write("Rojo\n"
                        + "Verde\n"
                        + "Amarillo\n"
                        + "Azul\n"
                        + "Blanco\n"
                        + "Morado");
                archi.close();
            }
        } catch (IOException e) {

        }
    }

    public String[] leerColores() {
        String[] marcas = new String[10];
        try {
            FileReader read = new FileReader("Colores.txt");
            BufferedReader conten = new BufferedReader(read);
            String texto = "";
            int cont = 0;
            while (conten.ready()) {
                texto = conten.readLine();
                marcas[cont] = texto;
                cont++;
            }
            read.close();
            conten.close();
        } catch (IOException e) {
            System.out.println("Error al escribir en el archivo y LEERLO" + e);
        }
        return marcas;
    }

    public void capturarDatosFu(Furgoneta furgoneta) {
        for (int i = 0; i < furgonetas.length; i++) {
            if (furgonetas[i] == null) {
                furgonetas[i] = furgoneta;
                break;
            }
        }
    }

    public void capturarDatosDe(Deportivo deportivo) {
        for (int i = 0; i < deportivos.length; i++) {
            if (deportivos[i] == null) {
                deportivos[i] = deportivo;
                break;
            }
        }
    }

    public void leerDepor(String datos) {
        File archivo = new File("Deportivo.txt");
        if (!archivo.exists()) {
            try {
                archivo.createNewFile();
            } catch (Exception e) {
            }
        } else {

        }
        try (FileWriter archi = new FileWriter(archivo, true)) {
            archi.write(datos);
            archi.close();
        } catch (IOException e) {

        }
    }

    public String[] leerDatos() {
        String[] marcas = new String[10];
        try {
            FileReader read = new FileReader("Deportivo.txt");
            BufferedReader conten = new BufferedReader(read);
            String texto = "";
            int cont = 0;
            while (conten.ready()) {
                texto = conten.readLine();
                marcas[cont] = texto;
                cont++;
            }
            read.close();
            conten.close();
        } catch (IOException e) {
            System.out.println("Error al escribir en el archivo y LEERLO" + e);
        }
        return marcas;
    }

    public void leerFurgo(String datos) {
        File archivo = new File("Deportivo.txt");
        if (!archivo.exists()) {
            try {
                archivo.createNewFile();
            } catch (Exception e) {
            }
        } else {

        }
        try (FileWriter archi = new FileWriter(archivo, true)) {
            archi.write(datos);
            archi.close();
        } catch (IOException e) {

        }
    }

    public void escribir(String datos, LinkedList<String> llenarArray, String pla) {
        File archivo = new File("Deportivo.txt");
        if (archivo.exists()) {
            System.out.println("hi");
            archivo.delete();
        }
//        System.out.println(archivo.exists());
//        try {
//            FileWriter fw = new FileWriter(archivo);
//        } catch (IOException e) {
//            System.out.println("File is open");
//        }
//
//        if (!archivo.exists()) {
//            try {
//                archivo.createNewFile();
//            } catch (Exception e) {
//            }
//        } else {
//
//        }
//        for (String lista : llenarArray) {
//            try (FileWriter archi = new FileWriter(archivo, true)) {
//                String[] data = lista.split(" ");
//                String placa = data[1];
//                if (!placa.equalsIgnoreCase(pla)) {
//                    archi.write(lista);
//                    archi.close();
//                }
//            } catch (IOException e) {
//
//            }
//        }
//        try (FileWriter archi = new FileWriter(archivo, true)) {
//            archi.write(datos);
//            archi.close();
//        } catch (Exception e) {
//
//        }
    }
}
