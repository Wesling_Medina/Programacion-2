/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vehiculos;

import java.util.LinkedList;

/**
 *
 * @author Wesling Medina
 */
public class Main {
    
    public static void main(String[] args) {
        Logica v = new Logica();
        LinkedList<String> array = new LinkedList<>();
        array.add("Depor 123 hi rojo 200 30");
        array.add("Furgoneta 1234 hi amarillo 230 29\n");
        array.add("Depor 12345 hello blanco 210 39\n");
        array.add("Depor 1234 buu blanco 230 39\n");
        v.escribir("Furgoneta 123 hi negro 200 30", array, "123");
    }
}
