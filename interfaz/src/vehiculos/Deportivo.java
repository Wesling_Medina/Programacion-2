/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package vehiculos;

/**
 *
 * @author Wesling Medina
 */
public class Deportivo extends Vehiculo {

    int potencia;
    int velocidadMax;

    public Deportivo() {

    }

    public Deportivo(int potencia, int velocidadMax) {
        this.potencia = potencia;
        this.velocidadMax = velocidadMax;
    }

    public Deportivo(int potencia, int velocidadMax, String marca, String color, int precio, String placa) {
        super(marca, color, precio, placa);
        this.potencia = potencia;
        this.velocidadMax = velocidadMax;
    }

    public int getPotencia() {
        return potencia;
    }

    public void setPotencia(int potencia) {
        this.potencia = potencia;
    }

    public int getVelocidadMax() {
        return velocidadMax;
    }

    public void setVelocidadMax(int velocidadMax) {
        this.velocidadMax = velocidadMax;
    }

}
