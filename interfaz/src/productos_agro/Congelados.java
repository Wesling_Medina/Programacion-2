/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package productos_agro;

import java.util.Date;

/**
 *
 * @author Wesling Medina
 */
public class Congelados extends Productos {

    private int tRecomendada;

    public Congelados() {

    }

    public Congelados(Date fCaducidad, String lote, String pOrigen, Date fEnvasado, int tRecomendada) {
        super(fCaducidad, lote, pOrigen, fEnvasado);
        this.tRecomendada = tRecomendada;
    }

    public String getDatos() {
        return "Fecha caducidad: " + fCaducidad
                + "\nNúmero lote: " + lote
                + "\nPaís origen: " + pOrigen
                + "\nFecha envasado: " + fEnvasado
                + "\nTemperatura recomendada: " + tRecomendada;
    }

    public int gettRecomendada() {
        return tRecomendada;
    }

    public void settRecomendada(int tRecomendada) {
        this.tRecomendada = tRecomendada;
    }

}
