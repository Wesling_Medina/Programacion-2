/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package productos_agro;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import javax.swing.JOptionPane;

/**
 *
 * @author Wesling Medina
 */
public class Productos {

    public Date fCaducidad;
    public String lote;
    public String pOrigen;
    public Date fEnvasado;

    public Productos() {
    }

    public Productos(Date fCaducidad, String lote, String pOrigen, Date fEnvasado) {
        this.fCaducidad = fCaducidad;
        this.lote = lote;
        this.pOrigen = pOrigen;
        this.fEnvasado = fEnvasado;
    }

    public String fecha(Date fechaDate) {
        GregorianCalendar calendar = new GregorianCalendar();
        calendar.setTime(fechaDate);
        String dia = Integer.toString(calendar.get(Calendar.DATE));
        String year = Integer.toString(calendar.get(Calendar.YEAR));
        String mes = Integer.toString(calendar.get(Calendar.MONTH) + 1);
        return dia +"/"+mes+"/"+year;
    }

    public String getDatos() {
        return "Fecha caducidad: " + fecha(fCaducidad)
                + "\nNúmero lote: " + lote
                + "\nPaís origen: " + pOrigen
                + "\nFecha envasado: " + fecha(fEnvasado);
    }

    public String getpOrigen() {
        return pOrigen;
    }

    public void setpOrigen(String pOrigen) {
        this.pOrigen = pOrigen;
    }

    public String getLote() {
        return lote;
    }

    public void setLote(String lote) {
        this.lote = lote;
    }

    public Date getfCaducidad() {
        return fCaducidad;
    }

    public void setfCaducidad(Date fCaducidad) {
        this.fCaducidad = fCaducidad;
    }

    public Date getfEnvasado() {
        return fEnvasado;
    }

    public void setfEnvasado(Date fEnvasado) {
        this.fEnvasado = fEnvasado;
    }

}
