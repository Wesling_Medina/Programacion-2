/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package productos_agro;

import java.util.Date;

/**
 *
 * @author Wesling Medina
 */
public class Refrigerados extends Productos {

    private String cOSA;
    private String tRecomendada;

    public Refrigerados() {
    }
    
    public Refrigerados(Date fCaducidad, String lote, String pOrigen, Date fEnvasado, String cOSA, String tRecomendada) {
        super(fCaducidad, lote, pOrigen, fEnvasado);
        this.cOSA = cOSA;
        this.tRecomendada = tRecomendada;
    }

    public String getDatos() {
        return "Fecha caducidad: " + fCaducidad
                + "\nNúmero lote: " + lote
                + "\nPaís origen: " + pOrigen
                + "\nFecha envasado: " + fEnvasado
                + "\nCódigo de supervisión alimentaria: " + cOSA
                + "\nTemperatura recomendada: " + tRecomendada;
    }

    public String getcOSA() {
        return cOSA;
    }

    public void setcOSA(String cOSA) {
        this.cOSA = cOSA;
    }

    public String gettRecomendada() {
        return tRecomendada;
    }

    public void settRecomendada(String tRecomendada) {
        this.tRecomendada = tRecomendada;
    }
}
