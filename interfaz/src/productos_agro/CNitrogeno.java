/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package productos_agro;

import java.util.Date;

/**
 *
 * @author Wesling Medina
 */
public class CNitrogeno extends Congelados {

    private String mCE;
    private int tExpoNitro;

    public CNitrogeno(String mCE, int tExpoNitro, Date fCaducidad, String lote, String pOrigen, Date fEnvasado, int tRecomendada) {
        super(fCaducidad, lote, pOrigen, fEnvasado, tRecomendada);
        this.mCE = mCE;
        this.tExpoNitro = tExpoNitro;
    }

    public String getDatos() {
        return "CONGELADOS CON NITROGENO."
                + "\nFecha caducidad: " + fCaducidad
                + "\nNúmero lote: " + lote
                + "\nPaís origen: " + pOrigen
                + "\nFecha envasado: " + fEnvasado
                + "\nMétodo de congelación: " + mCE
                + "\nTiempo exposición al nitrogeno: " + tExpoNitro;
    }

    public String getmCE() {
        return mCE;
    }

    public void setmCE(String mCE) {
        this.mCE = mCE;
    }

    public int gettExpoNitro() {
        return tExpoNitro;
    }

    public void settExpoNitro(int tExpoNitro) {
        this.tExpoNitro = tExpoNitro;
    }

}
