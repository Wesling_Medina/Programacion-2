/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package productos_agro;

import java.util.Date;

/**
 *
 * @author Wesling Medina
 */
public class Frescos extends Productos {

    public Frescos() {
    }

    public Frescos(Date fCaducidad, String lote, String pOrigen, Date fEnvasado) {
        super(fCaducidad, lote, pOrigen, fEnvasado);
    }

    public String getDatos() {
        return "Fecha caducidad: " + fCaducidad
                + "\nNúmero lote: " + lote
                + "\nPaís origen: " + pOrigen
                + "\nFecha envasado: " + fEnvasado;
    }
}
